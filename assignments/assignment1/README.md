<img src="others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# Assignment 1: File and Message Sharing using Sockets

In this assignment, we will be building a file-/message-sharing client-server application using Sockets.

Sockets are the standard way to write programs that communicate over a network. While originally developed for Unix computers programmed in C, the socket abstraction is general and not tied to any specific operating system or programming language. This allows programmers to use the socket mental model to write correct network programs in many contexts.

This part of the assignment will give you experience with basic socket programming. You will write two pairs of TCP client and server programs for sending and receiving text messages over a virtual network (see figure below). One client/server pair is written in C, while the other pair is written in Python.

The client and server programs in both languages should meet the following specifications. Be sure to read these meticulously before and after programming to make sure your implementation fulfills them:

### Server specification
- Each server program should listen on a socket, wait for a client to connect, receive a message from the client, print the message to stdout, and then wait for the next client indefinitely.
- Each server should take one command-line argument: the port number to listen on for client connections.
- Each server should accept and process client communications in an infinite loop, allowing multiple clients to send messages to the same server. The server should only exit in response to an external signal (e.g., `SIGINT` from pressing `ctrl-c`).
- Each server should maintain a short (5-10) client queue and handle multiple client connection attempts sequentially. In real applications, a TCP server would fork a new process to handle each client connection concurrently, but that is not necessary for this assignment.
- Each server should gracefully handle error values potentially returned by socket programming library functions (see specifics for each language below). Errors related to handling client connections should not cause the server to exit after handling the error; all others should.

### Client specification
- Each client program should contact a server, read a message from stdin, send the message, and exit.
- Each client should read and send the message exactly as it appears in `stdin` until reaching an `EOF` (end-of-file).
Each client should take two command-line arguments: the server's IP address and the port number of the server.
- Each client must be able to handle arbitrarily large messages by iteratively reading and sending chunks of the message, rather than reading the whole message into memory first.
- Each client should handle partial sends (when a socket only transmits part of the data given in the last send call) by attempting to re-send the rest of the data until it has all been sent.
- Each client should gracefully handle error values potentially returned by socket programming library functions.

## Getting Started

### Virtual environment and network topology
This assignment builds on [assignment0](../assignment0), so please ensure that you have a successfully working virtual environment. We will be building a slightly different network this time, as depicted in the following figure.

<img src="../../others/images/assg1-topo.png" alt="Network Topology." width="550">

### Client-server code

We have provided scaffolding code in the `assignments/assignment1/` directory.
*You should read and understand this code before starting to program.*

You should program only in the locations of the provided files marked with `TODO` comments. There is one `TODO` section per client and one per server. You can add functions if you wish, but do not change file names, as they will be used for automated testing.

The following sections provide details for the client and server programs in each language.

#### - C
The classic "Beej's Guide to Network Programming" is located here: [https://beej.us/guide/bgnet/html/](https://beej.us/guide/bgnet/html/).
The [system call section](https://beej.us/guide/bgnet/html/#system-calls-or-bust) and [client/server section](https://beej.us/guide/bgnet/html/#client-server-background) will be most relevant. The man pages are also useful for looking up individual functions (e.g., `man socket`).

The files `client-c.c` and `server-c.c` contain scaffolding code. You will need to add socket programming and I/O code in the `TODO` locations. The reference solutions have roughly 70 (well commented and spaced) lines of code in the `TODO` sections of each file. Your implementations may be shorter or longer.

For error handling, you can call `perror` for socket programming functions that set the global variable `errno` (Beej's Guide will tell you which do). For those that don't, simply print a message to standard error.

You should build your solution by running `make` in the `assignments/assignment1` directory. Your code *must* build using the provided `Makefile`. The server should be run as `./server-c (port) > (output file)`. The client should be run as `./client-c (server IP) (server port) < (message file)`. See "Testing" for more details.

#### - Python
Python socket programming documentation is located here: [https://docs.python.org/2/library/socket.html](https://docs.python.org/2/library/socket.html). The first few paragraphs at the top, the [section on socket objects](https://docs.python.org/2/library/socket.html#socket-objects) and the [first example](https://docs.python.org/2/library/socket.html#example) are particularly relevant.

The files `client-python.py` and `server-python.py` contain the scaffolding code. You will need to add socket programming code in the locations marked `TODO`. The reference solutions have roughly 15 (well commented and spaced) lines of code in the `TODO` sections of each file. Your implementations may be shorter or longer.

The Python socket functions will automatically raise exceptions with helpful error messages. No additional error handling is required.

The server should be run as `python server-python.py (port) > (output file)`. The client should be run as `python client-python.py (server IP) (server port) < (message file)`. See [Testing](#testing) for more details.

### Testing

You can test your implementations by attempting to send messages from your clients to your servers in the virtual environment. Either `h1` or `h2` can act as a server or client. You should use the host's IP address running the server and high server port number between 10000 and 60000. You can kill the server by pressing `ctrl-c`.

The Bash script `tests/client-server.sh` will test your implementation by attempting to send several different messages between all 4 combinations of your clients and servers (C client to C server, C client to Python server, etc.). The messages are the following:

1. The short message "Go Boilermakers!\n"
2. A long, randomly generated alphanumeric message
3. A long, randomly generated binary message
4. Several short messages sent sequentially from separate clients to one server
5. Several long, random alphanumeric messages sent concurrently from separate clients to one server

From either `h1` or `h2`, run the script as ...

```sh
$ ./tests/client-server.sh python (server port)
```

> **Note:** Remember to log into the virtual host using the `make host-h*` command (see [assignment0](../assignment0)).

If you get a permissions error, run `chmod 744 tests/client-server.sh` to give the script execute privileges.

For each client/server pair, the test script will print "SUCCESS" if the message is sent and received correctly. Otherwise, it will print a diff of the sent and received message if the diff output is human-readable, i.e., just for tests 1 and 4.

### Debugging hints

Here are some debugging tips. If you are still having trouble, ask a question on [Campuswire](https://www.gradescope.com/courses/227978) or see an instructor during office hours.

* There are defined buffer size and queue length constants in the scaffolding code. Use them. If they are not defined in a particular file, you don't need them. If you are not using one of them, either you have hard-coded a value, which is bad style, or you are very likely doing something wrong.
* There are multiple ways to read and write from `stdin`/`stdout` in C and Python. Any method is acceptable as long as it does not read an unbounded amount into memory at once and does not modify the message.
* If you are using buffered I/O to write to stdout, make sure to call `flush` or the end of a long message may not write.
* Remember to close the socket at the end of the client program.
* If you get "address already in use" errors, make sure you don't already have a server running. Otherwise, restart your virtual environment as described in [assignment0](../assignment0).  
* If you are getting other connection errors, try a different port between 10000 and 60000.

### Q&A

* **Should I set the `MSG_WAITALL` flag on `recv()`?** No. This causes `recv()` to not return until it receives a specified amount of data. However, the server cannot know this amount in advance, so you should instead keep calling `recv()` until there is nothing left to receive.
* **Do I need to handle signals such as SIGINT to clean up the server process when the user presses `ctrl-c`?** No, it is not necessary for this assignment. The default response to signals is good enough. Keep in mind it would be good practice to do so in general.
* **Should I use stream or datagram sockets?** Please use stream sockets to ensure that the exact message is delivered. Datagram packets are not guaranteed to be delivered.
* **Should the client wait to receive a reply from the server?** No, in this assignment, it should exit immediately after sending all the data.
* **Should the server handle client connections concurrently (in separate processes)?** No, as stated in the client specification, this is not required in this assignment. So no need to use `fork()`!

## Submission and Grading
Submit the assignment by uploading your modified client and server files to [Brightspace](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5397012/View).
You can submit as many times as you like before the deadline, but we will only take the last submission.

We will grade your assignments by running the `tests/client_server.sh` script and additional tests with large messages, multiple simultaneous clients, etc. Double-check the specifications above and perform your own tests before submitting them.

Code that does not compile is graded harshly; if you want partial credit on code that doesn't compile, comment it out and make sure your file compiles!

Remember that, in addition to your C client/server pair, you should submit another client/server in Python!


## Acknowledgement

This assignment is modeled after a [similar assignment](https://github.com/PrincetonUniversity/COS461-Public/tree/master/assignments/assignment1) offered at Princeton University by my Ph.D. advisor, Nick Feamster.
