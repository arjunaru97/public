<img src="others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# Welcome to CS 536: Data Communication And Computer Networks!

Hello and welcome to CS 536, a graduate-level course on data communication and computer networks at Purdue University!

My name is [Muhammad Shahbaz](https://mshahbaz.gitlab.io/), and I will be your instructor for this course during this semester, accompanied by our amazing TA, [Tao Li](https://tao.li/).

These are tumultuous times that have affected all parts of our lives---from families to work to outings and more. Still, we are here with all of what's happening around us, making progress bit-by-bit of each passing day. For me, and I am sure for all of you too, **Internet** has a powerful role to play here---in keeping us in touch with loved ones and letting us continue with work, albeit remotely. Take this class, for example; we wouldn't be meeting here today (from the safety of our homes) without the Internet! If you think of it, it's just mind-boggling how the Internet is keeping billions of us, humans, in touch with each other and entertained through voice, video, and text.

But, it's not all magic, and my **aim** in this course is to help demystify the mysteries of the Internet---the underlying principles and mechanisms that make it tick!

Our journey will start from our laptops (or desktops), including mobile devices, that need to communicate with each other. We will see how our homes are already serving as a mini-Internet, connecting our laptops, TVs, phones, cameras, and other household appliances with each other and the outside world. A Home Router is the backbone of this mini-Internet, keeping everything interconnected and secure. Stepping outside the house, our next stop will be an Internet Service Provider (ISP). These ISPs serve the same role as the home router; however, instead of connecting devices within a home, these ISPs are tasked with connecting homes (and businesses) together in a larger community, ranging from counties, cities, states, to as far as countries and worlds---ever wondered how Mars rover communicates with NASA!

We will also visit some new and exciting places during our journey! You may have heard of Google, Microsoft, Facebook, NetFlix, and more -- *if you haven't, I'd be curious to know who you are. :-)* In their early days, these places were no bigger than a garage, but, over time, they added more and more servers and computers (to handle the increasing amount of data) and transformed into what we call **data centers** today. We will learn more about them and new approaches to managing them (e.g., software-defined networks and programmable hardware) in the latter half of our course.

My hope is that, through this journey, you will have a stronger grasp of the Internet: its various elements and the tools and tricks used to make it scale!

I look forward to seeing you all on Wednesday. Happy New Year, and I hope you have a fun and enjoyable semester!

## Course logistics

**This will be the first and only announcement you will hear from me on [Brightspace](https://purdue.brightspace.com/d2l/home/209528).** Here onwards, we will be using [Campuswire](https://campuswire.com/c/G1FDC14E5) as our primary mode of communication. (Please see below for instructions on joining it and other tools we will be using throughout the semester.)

The course syllabus and schedule will be maintained on the following GitLab repository: [https://gitlab.com/purdue-cs536/public](https://gitlab.com/purdue-cs536/public). We will post everything related to the course there. So, please follow it closely throughout the course.

The course will have the following components:
1. About 3 lectures per week
2. 6 paper discussions
3. 2 programming assignments
4. A course project -- **the most important and challenging aspect of the course, so start on this early**
5. A midterm and a final exam

Visit the course page on [GitLab](https://gitlab.com/purdue-cs536/public) for grading and additional details on each of these components.

## A little about myself

I am a newly minted professor at Purdue ([https://mshahbaz.gitlab.io/](https://mshahbaz.gitlab.io/)). My previous academic homes were Stanford (as a postdoc) and Princeton (as a graduate student). I look forward to working with you all this semester!
